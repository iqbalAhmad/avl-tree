#include "quetype.h"
#include <iostream>
using namespace std;

template <class T>
QueType<T>::QueType()
{
    front=rear=NULL;
    length = 0;

}
template <class T>
QueType<T>::~QueType()
{
    MakeEmpty();
}
template <class T>
bool QueType<T>::IsFull()
{
    NodeType<T>* temp;
    try
    {
        temp = new NodeType<T>;
        delete temp;
        return false;
    }
    catch(...)
    {
        return true;
    }
}
template <class T>
bool QueType<T>::IsEmpty()
{
    return (front==NULL);
}
template <class T>
void QueType<T>::Enqueue(T item)
{
    if(IsFull())
        throw FullQueue();
    else
    {
        NodeType<T>* temp = new NodeType<T>;
        temp->info = item;
        temp->next = NULL;

        if(rear ==NULL)
        {
            front = temp;
        }
        else
        {
            rear->next = temp;
        }
        rear = temp;
    }
}
template <class T>
void QueType<T>::Dequeue(T& item)
{
    if(IsEmpty())
    {
        throw EmptyQueue();
    }
    else
    {
        NodeType<T>* temp;
        temp = front;
        item = temp->info;
        front = front->next;

        if(front==NULL)
            rear = NULL;
        delete temp;
    }

}
template<class T>
void QueType<T>::MakeEmpty()
{
    NodeType<T>* temp;
    while(front!=NULL)
    {
        temp = front;
        front = front->next;
        delete temp;
    }
    rear = NULL;
}
template<class T>
int QueType<T>::GetLength()
{
    NodeType<T>* temp;
    temp = front;

    while(temp!=NULL)
    {
        length++;
        temp=temp->next;
    }
    return length;

}
template <class T>
void QueType<T>::PrintQueue()
{
    NodeType<T>* temp;
    temp = front;
    while(temp!=NULL)
    {
        cout<<temp->info<<' ';
        temp = temp->next;
    }
    cout<<endl;
}
