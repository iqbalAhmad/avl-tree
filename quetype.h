#ifndef QUETYPE_H_INCLUDED
#define QUETYPE_H_INCLUDED

template <class T>
struct NodeType
{
    T info;
    NodeType* next;
};

class FullQueue{};
class EmptyQueue{};

template <class T>
class QueType
{
public:
    QueType();
    ~QueType();
    void MakeEmpty();
    void Enqueue(T item);
    void Dequeue(T& item);
    bool IsEmpty();
    bool IsFull();
    int GetLength();
    void PrintQueue();
private:
    NodeType<T>* front;
    NodeType<T>* rear;
    int length;
};


#endif // QUETYPE_H_INCLUDED
