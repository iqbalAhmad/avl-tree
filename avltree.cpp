#include "avltree.h"
#include <queue>    //#include "quetype.cpp"
#include <iostream>
using namespace std;

template <class ItemType>
TreeType<ItemType>::TreeType()
{
    root = NULL;
}

template <class ItemType>
void Destroy(TreeNode<ItemType>*& tree)
{
    if(tree != NULL)
    {
        Destroy(tree->left);
        Destroy(tree->right);
        delete tree;
        tree = NULL;
    }
}

template <class ItemType>
TreeType<ItemType>::~TreeType()
{
    Destroy(root);
}

template <class ItemType>
void TreeType<ItemType>::MakeEmpty()
{
    Destroy(root);
}



//----------------AVL----------------------------------------
//---------------START HERE----------------------------------
//-----------------------------------------------------------

//---------------Declaring functions------------------------
template <class ItemType>
int height(TreeNode<ItemType>* root);

template <class ItemType>
int difference(TreeNode<ItemType>* temp);

template <class ItemType>
TreeNode<ItemType>* RightRight_rot(TreeNode<ItemType>* root);

template <class ItemType>
TreeNode<ItemType>* LeftLeft_rot(TreeNode<ItemType>* root);

template <class ItemType>
TreeNode<ItemType>* LeftRight_rot(TreeNode<ItemType>* root);

template <class ItemType>
TreeNode<ItemType>* RightLeft_rot(TreeNode<ItemType>* root);

template <class ItemType>
TreeNode<ItemType>* balance(TreeNode<ItemType>* temp);


//----------------------------Height function (local function)----------
template <class ItemType>
int height(TreeNode<ItemType>* root)
{
    if(root == NULL) return -1;
    else
    {
        int leftH = height(root->left);
        int rightH = height(root->right);

        //-------------------Max---------
        if(leftH > rightH)
            return (leftH+1);
        else
            return (rightH+1);
    }
}



//----------------------------Difference of two height()----------
template <class ItemType>
int difference(TreeNode<ItemType>* temp)
{
    int LeftTree = height(temp->left);
    int RightTree = height(temp->right);
    return (LeftTree - RightTree);
}

//------------------------Right-Right rotation (local function)------------
template <class ItemType>
TreeNode<ItemType>* RightRight_rot(TreeNode<ItemType>* root)

{
    TreeNode<ItemType>* temp;
    temp = root->right;
    root->right = temp->left;
    temp->left = root;

    return temp;
}

//----------------------- Left-Left rotation (local function)-------------
template <class ItemType>
TreeNode<ItemType>* LeftLeft_rot(TreeNode<ItemType>* root)

{
    TreeNode<ItemType>* temp;
    temp = root->left;
    root->left = temp->right;
    temp->right = root;
    return temp;
}

 //-------------------------Left-Right rotation(Local function)----------
 template <class ItemType>
TreeNode<ItemType>* LeftRight_rot(TreeNode<ItemType>* root) //

{
    TreeNode<ItemType>* temp;
    temp = root->left;
    root->left = RightRight_rot(temp);

    return LeftLeft_rot(root);
}

//--------------------------Right-Left rotate (Local function)--------------
 template <class ItemType>
TreeNode<ItemType>* RightLeft_rot(TreeNode<ItemType>* root)

{
   TreeNode<ItemType>* temp;
    temp = root->right;
    root->right = LeftLeft_rot(temp);
    return RightRight_rot(root);
}

//-------------------------Balancing the tree (local function)---------------
 template <class ItemType>
TreeNode<ItemType>* balance(TreeNode<ItemType>* temp)

{
    int diff = difference(temp);
    if(diff > 1)
    {
        if(difference(temp->left) > 0)
            temp = LeftLeft_rot(temp);
        else
            temp = LeftRight_rot(temp);
    }
    else if(diff < -1)
    {
        if(difference(temp->right) > 0)
             temp = RightLeft_rot(temp);
        else
            temp = RightRight_rot(temp);
    }
    return temp;
}


//------------------------------Insert functions------------
template <class ItemType>
void Insert(TreeNode<ItemType>*& tree, ItemType item)
{
    if(tree == NULL) // Tree is empty or null.
    {
        tree = new TreeNode<ItemType>;
        tree->right = NULL;
        tree->left = NULL;
        tree->info = item;
    }
    else if(item < tree->info){
        Insert(tree->left, item);
        tree = balance(tree);
    }
    else{
        Insert(tree->right, item);
        tree = balance(tree);

    }
}

template <class ItemType>
void TreeType<ItemType>::InsertItem(ItemType item)
{
    Insert(root, item);
}


//---------------------------DELETE FUNCTION------------------

template <class ItemType>
void Delete(TreeNode<ItemType>*& tree, ItemType item)

{
    if(item < tree->info)
        Delete(tree->left, item);
    else if(item > tree->info)
        Delete(tree->right, item);
    else{
        DeleteNode(tree);
        tree = balance(tree);   // Balancing tree-----
    }

}

template <class ItemType>
void DeleteNode(TreeNode<ItemType>* &tree)

{
    ItemType data;
    TreeNode<ItemType>* tempPtr;

    tempPtr =tree;
    if(tree->left == NULL)
    {
        tree = tree->right;
        delete tempPtr;

    }
    else if(tree->right == NULL)
    {
        tree = tree->left;
        delete tempPtr;


    }
    else
    {
        GetPredecessor(tree->left, data);
        tree->info = data;
        Delete(tree->left, data);
    }

}

template <class ItemType>
void TreeType<ItemType>::DeleteItem(ItemType item)
{
    Delete(root, item);
}
//-------------------------------------END-------------------------------------------------
//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------

template <class ItemType>
bool TreeType<ItemType>::IsEmpty()
{
    return root == NULL;
}

template <class ItemType>
bool TreeType<ItemType>::IsFull()
{
    TreeNode<ItemType>* location;
    try
    {
        location = new TreeNode<ItemType>;
        delete location;
        return false;
    }
    catch(bad_alloc& exception)
    {
        return true;
    }
}

template <class ItemType>
int CountNodes(TreeNode<ItemType>* tree)
{
    if(tree == NULL)
        return 0;
    else
        return CountNodes(tree->left)+ CountNodes(tree->right) + 1;
}

template <class ItemType>
int TreeType<ItemType>::LengthIs()
{
    return CountNodes(root);
}

template <class ItemType>
void Retrieve(TreeNode<ItemType>* tree, ItemType& item, bool& found)
{
    if(tree == NULL)
        found = false;
    else if(item < tree->info)
        Retrieve(tree->left, item, found);
    else if(item > tree->info)
        Retrieve(tree->right, item, found);
    else
    {
        item = tree->info;
        found = true;
    }
}

template <class ItemType>
void TreeType<ItemType>::RetrieveItem(ItemType& item, bool& found)
{
    Retrieve(root, item, found);
}

/*
template <class ItemType>
void Insert(TreeNode<ItemType>*& tree, ItemType item)
{
    if(tree == NULL)
    {
        tree = new TreeNode<ItemType>;
        tree->right = NULL;
        tree->left = NULL;
        tree->info = item;
    }
    else if(item < tree->info)
        Insert(tree->left, item);
    else
        Insert(tree->right, item);
}

template <class ItemType>
void TreeType<ItemType>::InsertItem(ItemType item)
{
    Insert(root, item);
}

*/
/*
template <class ItemType>
void Delete(TreeNode<ItemType>*& tree, ItemType item)

{
    if(item < tree->info)
        Delete(tree->left, item);
    else if(item > tree->info)
        Delete(tree->right, item);
    else{
        DeleteNode(tree);
        tree = balance(tree); //----------Balancing tree-----
    }

}

template <class ItemType>
void DeleteNode(TreeNode<ItemType>* &tree)

{
    ItemType data;
    TreeNode<ItemType>* tempPtr;

    tempPtr =tree;
    if(tree->left == NULL)
    {
        tree = tree->right;
        delete tempPtr;

    }
    else if(tree->right == NULL)
    {
        tree = tree->left;
        delete tempPtr;


    }
    else
    {
        GetPredecessor(tree->left, data);
        tree->info = data;
        Delete(tree->left, data);
    }

}
*/

template <class ItemType>
void GetPredecessor(TreeNode<ItemType>* tree, ItemType& data)
{
    while(tree->right != NULL)
        tree = tree->right;
    data = tree->info;
}



template <class ItemType>
void PreOrder(TreeNode<ItemType>* tree, queue<ItemType> &Que)
{
    if(tree != NULL)
    {
        Que.Enqueue(tree->info);
        PreOrder(tree->left, Que);
        PreOrder(tree->right, Que);
    }
}

template <class ItemType>
void InOrder(TreeNode<ItemType>* tree, queue<ItemType> &Que)
{
    if(tree != NULL)
    {
        InOrder(tree->left, Que);
        Que.Enqueue(tree->info);
        InOrder(tree->right, Que);
    }
}

template <class ItemType>
void PostOrder(TreeNode<ItemType>* tree, queue<ItemType> &Que)
{
    if(tree != NULL)
    {
        PostOrder(tree->left, Que);
        PostOrder(tree->right, Que);
        Que.Enqueue(tree->info);
    }
}

template <class ItemType>
void TreeType<ItemType>::ResetTree(OrderType order)
{
    switch(order)
    {
        case PRE_ORDER:
            PreOrder(root, preQue);
            break;
        case IN_ORDER:
            InOrder(root,inQue);
            break;
        case POST_ORDER:
            PostOrder(root, postQue);
            break;
    }
}

template <class ItemType>
void TreeType<ItemType>::GetNextItem(ItemType& item, OrderType order, bool& finished)
{
    finished = false;
    switch(order)
    {
    case PRE_ORDER:
        preQue.Dequeue(item);
        if(preQue.IsEmpty())
            finished = true;
        break;

    case IN_ORDER:
        inQue.Dequeue(item);
        if(inQue.IsEmpty())
            finished = true;
        break;

    case POST_ORDER:
        postQue.Dequeue(item);
        if(postQue.IsEmpty())
            finished = true;
        break;
    }
}


template <class ItemType>
void PrintTree(TreeNode<ItemType>* tree)
{
    if(tree != NULL)
    {
        PrintTree(tree->left);
        cout << tree->info << " ";
        PrintTree(tree->right);
    }
}

template <class ItemType>
void TreeType<ItemType>::Print()
{
    PrintTree(root);

}

//---------------------------New Functions for print---------------
template <class ItemType>
void PrintTreePreOrder(TreeNode<ItemType>* tree)
{
    if(tree != NULL)
    {
        cout << tree->info << " ";
        PrintTreePreOrder(tree->left);
        PrintTreePreOrder(tree->right);
    }
}

template <class ItemType>
void TreeType<ItemType>::PrintPreOrder()
{
    PrintTreePreOrder(root);

}

template <class ItemType>
void PrintTreePostOrder(TreeNode<ItemType>* tree)
{
    if(tree != NULL)
    {
        PrintTreePostOrder(tree->left);
        PrintTreePostOrder(tree->right);
        cout << tree->info << " ";
    }
}

template <class ItemType>
void TreeType<ItemType>::PrintPostOrder()
{
    PrintTreePostOrder(root);

}

