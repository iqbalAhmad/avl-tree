#ifndef AVLTREE_H_INCLUDED
#define AVLTREE_H_INCLUDED


#include <queue>    //#include "quetype.h"

using namespace std;

template <class ItemType>
struct TreeNode
{
    ItemType info;
    TreeNode* left;
    TreeNode* right;
};

enum OrderType {PRE_ORDER, IN_ORDER, POST_ORDER};

template <class ItemType>
class TreeType
{
public:
    TreeType();
    ~TreeType();
    void MakeEmpty();
    bool IsEmpty();
    bool IsFull();
    int LengthIs();
    void RetrieveItem(ItemType& item, bool& found);
    void InsertItem(ItemType item);
    void DeleteItem(ItemType item);
    void ResetTree(OrderType order);
    void GetNextItem(ItemType& item, OrderType order, bool& finished);
    void Print();
    void PrintPreOrder();
    void PrintPostOrder();
private:
    TreeNode<ItemType>* root;
    queue<ItemType> preQue;
    queue<ItemType> inQue;
    queue<ItemType> postQue;
};





#endif // AVLTREE_H_INCLUDED
